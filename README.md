# go-http-headers

This is a small webserver written in Golang that will return your http request headers (as json). This can be useful when debugging proxy servers.

The web server listens on port `7777` by default, but can be changed by setting the environment variable `PORT`.

Build it:

```
go build
```

Then run it:

```
./go-http-headers
```
