package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

// Resp main data sctructure
type Resp struct {
	Method     string
	Host       string
	RequestURI string
	Proto      string
	RemoteAddr string
	// URL        *url.URL
	RequestHeaders map[string]string
}

func main() {
	port, portexists := os.LookupEnv("PORT")

	if !portexists {
		port = "7777"
	}

	http.HandleFunc("/", handler)

	log.Println("Listening on port " + port + "...")
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	d := Resp{}

	// Iterate over all header fields
	m := make(map[string]string)
	for k, v := range r.Header {
		m[k] = v[0]
	}

	d.Method = r.Method
	d.Host = r.Host
	d.Proto = r.Proto
	d.RequestURI = r.RequestURI
	d.RemoteAddr = r.RemoteAddr
	// d.URL = r.URL
	d.RequestHeaders = m

	dataJSON, _ := json.Marshal(d)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(dataJSON)
	log.Println(r.Method, r.Host, r.RequestURI, r.RemoteAddr)
}
