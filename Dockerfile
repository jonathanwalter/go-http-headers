############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder

WORKDIR $GOPATH/src/go-http-headers/

COPY main.go .

RUN go build -o /go/bin/go-http-headers

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/go-http-headers

############################
# STEP 2 build a small image
############################
FROM scratch

# Copy our static executable.
COPY --from=builder /go/bin/go-http-headers /go/bin/go-http-headers

# Run the go-http-headers binary.
ENTRYPOINT ["/go/bin/go-http-headers"]